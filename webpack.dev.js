// -------------------------------------------------------------------
// This configuration file is for development use
// You can make changes to this file to suit your development need
// Please refer to this webpack documentation for further information
// https://webpack.js.org/guides/development/
// -------------------------------------------------------------------

const path = require('path');
const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const autoprefixer = require('autoprefixer');

module.exports = merge(common, {
  module: {
    rules: [
      {
        test: /\.(scss)$/,
        exclude: [path.resolve(__dirname, 'node_modules/font-awesome/scss/font-awesome.scss')],
        use: [
          {
            loader: 'style-loader', // creates style nodes from JS strings
          },
          {
            loader: 'css-loader', // translates CSS into CommonJS
            options: {
              modules: true,
              localIdentName: '[name]__[local]___[hash:base64:5]',
            },
          },
          {
            loader: 'postcss-loader', // provide autoprefixer in compiled CSS
            options: {
              plugins: () => [autoprefixer],
            },
          },
          {
            loader: 'sass-loader', // compiles Sass to CSS
            options: {
              modules: true,
              localIdentName: '[name]__[local]___[hash:base64:5]',
            },
          },
        ],
      },
    ],
  },
  devServer: {
    historyApiFallback: true,
    port: 3000,
    open: true,
  },
  mode: 'development',
  devtool: 'inline-source-map',
});
