# Product Catalog

Feature: browse products in the infinite list, view detailed information of each product
and also view the image of each product

## Getting Started

Clone this repo using git clone https://gitlab.com/hashas/product-catalog.git
Move to the appropriate directory: cd <YOUR_PROJECT_NAME>

```
Run npm install in order to install all dependencies (internet requeired)
```

Development Mode

```
Run npm run open:dev (development mode without linting)
Run npm run start (developent mode including linting)
```

Production Mode

```
Run npm run open:prod (or run build -- including linting)
```

### Prerequisites

What things you need to install the software and how to install them

```
Make sure that you have Node.js v8.15.1 and npm v5 or above installed.
```
