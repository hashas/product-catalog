/* eslint-disable no-shadow */
/* eslint-disable react/require-default-props */
// npm module import
import '@babel/polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
// redux import
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';
import './style/reset.scss';

import store from './store/store';

// app module import
import App from './app';

const Root = ({ store }) => (
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>
);

Root.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  store: PropTypes.object,
};

ReactDOM.render(<Root store={store} />, document.getElementById('root'));
