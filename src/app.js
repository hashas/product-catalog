// npm module import //
import React, { Fragment } from 'react';
import { Route } from 'react-router-dom';

// app module imports
import Catalog from './containers/catalog/Catalog';
import Detail from './containers/detail/Detail';
import Search from './containers/search/Search';
import Confirmation from './components/confirmation/Confirmation';

const App = () => (
  <Fragment>
    <Route path="/" exact component={Catalog} />
    <Route path="/detail" component={Detail} />
    <Route path="/search" component={Search} />
    <Confirmation />
  </Fragment>
);

export default App;
