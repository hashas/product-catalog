/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import
import { connect } from 'react-redux';
import React, { Component, Fragment } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { uid } from 'react-uid';
import { fetchProducts, SetTitle, Togle } from '../../store/actions/index';

// Import Component //
import Header from './../../components/header/Header';
import Filter from './../../components/filter/Filter';
import Cards from './../../components/cards/Cards';
import HeaderAction from './../../components/header/HeaderAction';

// Import Styling
import './catalog.scss';

// Get data from store //
const mapStateToProps = state => ({
  titleCategory: state.titleCategory,
  products: state.products,
  page: state.page,
  isFetching: state.isFetching,
});

class Catalog extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.handleToggle = this.handleToggle.bind(this);
  }

  handleAppend() {
    this.props.dispatch(fetchProducts());
  }

  handleToggle(item) {
    this.props.dispatch(Togle(item));
  }

  componentDidMount() {
    this.props.dispatch(SetTitle('Product List'));
  }

  render() {
    // Map data into cards //
    const cardList = this.props.products
      ? this.props.products.map(item => (
        <Cards
          products={item}
          handleViewDetail={this.handleViewDetail}
          key={uid(item)}
          id={item.id}
          handleToggle={this.handleToggle}
        />
      ))
      : [];

    return (
      <Fragment>
        <Header>
          <HeaderAction titleCategory={this.props.titleCategory} />
        </Header>
        <section className="section-container">
          <div role="main">
            <Filter />
            <InfiniteScroll
              pageStart={0}
              threshold={(480 * 6) / 2}
              loadMore={() => this.handleAppend()}
              hasMore={true || false}
              loader={
                <div className="loader">
                  <img
                    src="data:image/gif;base64,R0lGODlhQABAAKIHAOz08ZgHQs6fsr5wkKo4ZtvEzejo6P///yH/C05FVFNDQVBFMi4wAwEAAAAh/wtYTVAgRGF0YVhNUDw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMTQwIDc5LjE2MDQ1MSwgMjAxNy8wNS8wNi0wMTowODoyMSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTggKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OUM4QURGRkQwQ0UyMTFFOUI4QzdCRTA0REY1REU3MDgiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OUM4QURGRkUwQ0UyMTFFOUI4QzdCRTA0REY1REU3MDgiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo5QzhBREZGQjBDRTIxMUU5QjhDN0JFMDRERjVERTcwOCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo5QzhBREZGQzBDRTIxMUU5QjhDN0JFMDRERjVERTcwOCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PgH//v38+/r5+Pf29fTz8vHw7+7t7Ovq6ejn5uXk4+Lh4N/e3dzb2tnY19bV1NPS0dDPzs3My8rJyMfGxcTDwsHAv769vLu6ubi3trW0s7KxsK+urayrqqmop6alpKOioaCfnp2cm5qZmJeWlZSTkpGQj46NjIuKiYiHhoWEg4KBgH9+fXx7enl4d3Z1dHNycXBvbm1sa2ppaGdmZWRjYmFgX15dXFtaWVhXVlVUU1JRUE9OTUxLSklIR0ZFRENCQUA/Pj08Ozo5ODc2NTQzMjEwLy4tLCsqKSgnJiUkIyIhIB8eHRwbGhkYFxYVFBMSERAPDg0MCwoJCAcGBQQDAgEAACH5BAUDAAcALAAAAABAAEAAAANHaLrc/jDKSau9OOvNu/9gKI5kaZ5oqq5s675wLM90bd94ru987//AoHBILBqPyKRyyWw6n9CodEqtWq/YrHbL7Xq/4LDYkAAAIfkEBQMABwAsGAAZAAUABQAAAwx4sFpSSoSxyihAsQQAIfkEBQMABwAsGQAYAAQAAwAAAwh4cFdUZwQCEgAh+QQFAwAHACwYABYABgAEAAADC3gH2lMKjBCiCGQlACH5BAUDAAcALBgAEwAGAAYAAAMRaLDWJQWoEYgCNBQlgl7ZlgAAIfkEBQMABwAsFwAPAAcABwAAAxRoBtrcqxBSGBAhECv1UkTmDFqVAAAh+QQFAwAHACwVAA0ABwAFAAADEQg3VAoiyHNAIXK8GBo9WCAkACH5BAUDAAcALBEACQAJAAcAAAMXeHqwS0Q4IIIlpZ1CrBXaIXTWxDmOsSQAIfkEBQMABwAsCwAIAAoABgAAAxl4Csy6pYh5gCIh61qwDg8wEIQwDOBSNUoCACH5BAUDAAcALAcACQAHAAkAAAMaeHoAy0csUEgIrN7g7BXAsBWNRypCMDyTkgAAIfkEBQMABwAsBQAPAAcACwAAAyBoGlwGghHwBhMUkPaAJUW2BeETBUN2DopCEFhrlDJlJAAh+QQFAwAHACwHABYACgAKAAADIHhK2h2hAAcEZPQQOGYDBRR0lMU5BzCIgqdUBIaCaKYkACH5BAUDAAcALA0AGwAKAAoAAAMjeAraTYW5QIMA8ghSx8kFRw3RUgwV1GBCdTlAC2UfUzqrkgAAIfkEBQMABwAsEwAfAAsACwAAAyV4dtzO4Dkiogx42FZwBhtgEB5xhAfpFQ8wlJKLmRskDEXdgFYCACH5BAUDAAcALBoAJQAKAAsAAAMjeAfaHYI5FaB81TpBcpRA0VXDFY6acmaFswbFtRRCNC2AlAAAIfkEBQMABwAsHQAsAAkADAAAAyZ4qgL7wbwT6nBrVHIwKFXwAMLWncTmOFpaYKC1sMFwKk5xz/uRAAAh+QQFAwAHACwXAC4ACgAKAAADJHgHoL5HjKeACGE0BwbWj4UFxQYRoyAQ1TUG4fqGxUDYFMMsCQAh+QQFAwAHACwYACcACQAKAAADH3i6B7xkvBXEEuGBQvLrgdcMYWUoRTk4ChYQzSJNRwIAIfkEBQMABwAsHgAhAAoACgAAAyN4qtC74IjxThwhjGgPyVp0gVRTfFl5GN+nKiT3BIJTEd2TAAAh+QQFAwAHACwkABsACgAKAAADIni6B8xFvFZCGE4BYQPRWxcITtiRCmcRRTZ0bKYQXjvFTwIAIfkEBQMABwAsKQAVAAsACgAAAyV4utD7oAwB3qkkkFLZCGABCWAwdFdREpAKbl1VBpx1gJR9508CACH5BAUDAAcALC8ADwAHAAkAAAMceCAxqiQEcdSQAkAMQIlB0TFSdYCDRk5aFRBmAgAh+QQFAwAHACwrAAgACgAIAAADHWgK2l2jGOaCJYWCQmwggiZ0XrZs3kAtgudMBlkkACH5BAUDAAcALCUACAAJAAgAAAMdeKrQwE+IMU4MONgycgYdRhATiAnWowxFs0DvkQAAIfkEBQMABwAsIQALAAcACgAAAx54urQgwRUSDzg0xKdDOUU3AMCgNaQZCBdQDKNDLgkAIfkEBQMABwAsHwASAAcACQAAAx1oShFioDSiwGgDwBsyZA6ggYVmNEGhGALxrNCaAAAh+QQFAwAHACwfABkABwAIAAADGAhHQaUnyHIUC+/EWZcUCnBR2jEIpQZACQAh+QQFAwAHACwgAB4ABgAGAAADEHh6AjAhjPMiUSVKReKCSgIAIfkEBQMABwAsJAAlAAYABgAAAw54QdfxAxzymISyldx4AgAh+QQFAwAHACwkACcACQAHAAADF0ixAvZmMWdAkWFQIHDZhUJAjyOQJGUkACH5BAUDAAcALCkAKAAHAAYAAAMUeAesWkIUoIIl1QYCBtlE4SjUkQAAIfkEBQMABwAsLQAlAAYACQAAAxd4B9rVYATCSrgFkEvaFcwRDEAINguaAAAh+QQFAwAHACwsACIABwAGAAADFHgH2lBlrUKCUECEQDDdBWN0QZgAACH5BAUDAAcALCwAIQAHAAYAAAMReAraVewAEYhoJJwwouKeciUAIfkEBQMABwAsMQAjAAUABQAAAw14AHdU7YQw1CRNDHgSACH5BAUDAAcALDMAIwAEAAYAAAMNCKZlTmSYEIgZpChlEgAh+QQFAwAHACw1ACMABAAGAAADDQhgVsUkCBAkEGUplgAAIfkEBQMABwAsNgAjAAQABAAAAwl4cCdULAhABksAIfkEBQMABwAsNwAjAAMAAwAAAwZoBUd34yQAIfkEBQMABwAsAAAAAAEAAQAAAwJ4CQAh+QQFAwAHACwAAAAAAQABAAADAngJACH5BAUDAAcALAAAAAABAAEAAAMCeAkAIfkEBQMABwAsAAAAAAEAAQAAAwJ4CQAh+QQFAwAHACwAAAAAAQABAAADAngJACH5BAUDAAcALAAAAAABAAEAAAMCeAkAIfkEBQMABwAsAAAAAAEAAQAAAwJ4CQAh+QQFAwAHACwAAAAAAQABAAADAngJACH5BAUDAAcALAAAAAABAAEAAAMCeAkAIfkEBQMABwAsAAAAAAEAAQAAAwJ4CQAh+QQFAwAHACwAAAAAAQABAAADAngJACH5BAUDAAcALAAAAAABAAEAAAMCeAkAIfkEBQMABwAsAAAAAAEAAQAAAwJ4CQAh+QQFAwAHACwAAAAAAQABAAADAngJACH5BAUDAAcALAAAAAABAAEAAAMCeAkAIfkEBQMABwAsAAAAAAEAAQAAAwJ4CQAh+QQFAwAHACwAAAAAAQABAAADAngJACH5BAUDAAcALAAAAAABAAEAAAMCeAkAIfkEBQMABwAsAAAAAAEAAQAAAwJ4CQAh+QQFAwAHACwAAAAAAQABAAADAngJACH5BAUDAAcALAAAAAABAAEAAAMCeAkAIfkEBQMABwAsAAAAAAEAAQAAAwJ4CQAh+QQFAwAHACwAAAAAAQABAAADAngJADs="
                    alt="gif"
                  />
                </div>
              }
            >
              {cardList}
            </InfiniteScroll>
          </div>
        </section>
      </Fragment>
    );
  }
}

export default connect(mapStateToProps)(Catalog);
