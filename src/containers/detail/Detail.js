/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import
import { connect } from 'react-redux';
import React, { Component, Fragment } from 'react';
import { SetDetail, SetTitle, fetchDetail, Togle } from '../../store/actions/index';

// Import Component //
import Header from './../../components/header/Header';
import DetailCards from './../../components/detailcards/DetailCards';
import HeaderAction from './../../components/header/HeaderAction';

// Import Styling
import './detail.scss';

const find = require('lodash.find');

// Get data from store //
const mapStateToProps = state => ({
  productDetail: state.productDetail,
  products: state.products,
  title: state.titleCategory,
});

class Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false,
    };
  }

  handleViewDetail() {
    const currentState = !this.state.modalIsOpen;
    this.setState({ modalIsOpen: currentState });
  }

  handleToggle() {
    this.props.dispatch(Togle({
      urls: this.props.location.state.item.imgUrl,
      text: this.props.location.state.item.title,
      price: this.props.location.state.item.size,
    }));
  }

  componentDidMount() {
    const theId = this.props.location.search.split('=')[1]
      ? this.props.location.search.split('=')[1]
      : window.location.search.split('=')[1];
    const theState = !this.props.location.state;

    if (this.props.location.state) {
      this.props.dispatch(SetTitle(this.props.location.state.item.title));
    }

    // If ulr was copy pasted into browser //
    if (theState) {
      const theDetail = find(this.props.products, item => item.id.toString() === theId);
      this.props.dispatch(SetDetail(theDetail));

      //  if id was not in the initial state, fetch new data //
      const stat = !theDetail;
      if (stat) {
        this.props.dispatch(fetchDetail(theId));
        this.props.dispatch(SetTitle('Only image was fetched'));
      }
    }

    // scroll back to top //
    window.scrollTo(0, 0);
  }

  render() {
    const itemDetail = this.props.location.state
      ? this.props.location.state.item
      : this.props.productDetail;

    return (
      <Fragment>
        <Header>
          <HeaderAction titleCategory={this.props.title} />
        </Header>
        <section className="section-container">
          <div role="main">
            <DetailCards
              productDetail={this.props.productDetail}
              itemDetail={itemDetail}
              modalIsOpen={this.state.modalIsOpen}
              handleViewDetail={() => this.handleViewDetail()}
              handleToggle={() => this.handleToggle()}
            />
          </div>
        </section>
      </Fragment>
    );
  }
}

export default connect(mapStateToProps)(Detail);
