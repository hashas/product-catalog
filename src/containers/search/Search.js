/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import
import { connect } from 'react-redux';
import React, { Component, Fragment } from 'react';

// Import Component //
import Header from './../../components/header/Header';
import HeaderSearch from './../../components/header/HeaderSearch';

// Import Styling
import './search.scss';

// Get data from store //
const mapStateToProps = state => ({ products: state.products });

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Fragment>
        <Header>
          <HeaderSearch />
        </Header>
        <section className="section-container">
          <div role="main" />
        </section>
      </Fragment>
    );
  }
}

export default connect(mapStateToProps)(Search);
