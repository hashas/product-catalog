const imgDataConverter = (data) => {
  const initial = [];
  if (data) {
    data.map(item =>
      initial.push({
        id: item.id,
        title: item.author,
        size: 'L, M, S, XL',
        price: item.width,
        imgUrl: item.download_url,
      }));
  }

  return initial;
};

const imgOnly = (data, id) => ({
  id,
  title: 'Only image was fetched',
  size: 'L, M, S, XL',
  price: 2004,
  imgUrl: data,
});

export { imgDataConverter, imgOnly };
