// state
const state = {
  name: 'Has Priahadena',
  isOpen: false,
  isSleep: false,
  isFetching: false,
  fetched: true,
  page: 1,
  imgData: [],
  txtData: [],
  products: [
    {
      id: 0,
      title: 'Celana Hidana',
      size: 'L, M, S, XL',
      price: '159.000',
      imgUrl:
        'https://imager-next.freetls.fastly.net/images/resized/480/e2630084-01e1-44be-ade4-e7a800922947',
    },
    {
      id: 1,
      title: 'Adeli Abstrak Cape Midi Dress',
      size: 'L, M, S, XL',
      price: '159.000',
      imgUrl:
        'https://imager-next.freetls.fastly.net/images/resized/480/1c4b3c04-6748-41ad-8b11-2f304005f07a',
    },
    {
      id: 2,
      title: 'Midi Dress',
      size: 'L, M, S, XL',
      price: '159.000',
      imgUrl:
        'https://imager-next.freetls.fastly.net/images/resized/480/c8b2fef8-c3fb-4d9e-ac2b-4729b3105d2b',
    },
    {
      id: 3,
      title: 'Adeli Abstrak Cape',
      size: 'L, M, S, XL',
      price: '159.000',
      imgUrl:
        'https://imager-next.freetls.fastly.net/images/resized/480/0cd18526-a571-4657-9b30-d0bc236e975f',
    },
  ],
  titleCategory: 'Product List',
  productDetail: {
    // id: 2,
    // title: 'Adeli Abstrak Cape Midi Dress3',
    // size: 'L, M, S, XL',
    // price: '159.000',
    // imgUrl:
    //   'https://imager-next.freetls.fastly.net/images/resized/480/c8b2fef8-c3fb-4d9e-ac2b-4729b3105d2b',
  },
  productPreview: null,
};

export default state;
