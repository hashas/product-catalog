// import redux
import { createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import app from './reducer/app';

const loggerMiddleware = createLogger();

const store = createStore(app, composeWithDevTools(applyMiddleware(thunk, loggerMiddleware)));
export default store;
