/* eslint-disable no-param-reassign */
/* eslint-disable no-case-declarations */
import states from '../state/state';
import {
  SET_DETAIL,
  APPEND_ITEMS,
  UPDATE_PAGE,
  SET_TITLE,
  SET_TEMPIMG,
  SET_TEMPTXT,
  HANDLE_TOGGLE,
} from '../actions/actionTypes';

const app = (state = states, action) => {
  switch (action.type) {
    case SET_DETAIL:
      return Object.assign({}, state, {
        productDetail: action.item,
      });
    case SET_TITLE:
      return Object.assign({}, state, {
        titleCategory: action.item,
      });
    case UPDATE_PAGE:
      return Object.assign({}, state, {
        page: state.page + 1,
      });
    case APPEND_ITEMS:
      return Object.assign({}, state, {
        products: [...state.products, ...action.item],
      });

    case HANDLE_TOGGLE:
      return Object.assign({}, state, {
        isOpen: !state.isOpen,
        productPreview: action.item,
      });

    case SET_TEMPIMG:
      return Object.assign({}, state, {
        isFetching: true,
        fetched: false,
      });

    case SET_TEMPTXT:
      return Object.assign({}, state, {
        isFetching: false,
        fetched: true,
      });

    default:
      return state;
  }
};

export default app;
