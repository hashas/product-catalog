import service from './../../service/service';
import { imgDataConverter, imgOnly } from './../../helper/helper';

import {
  SET_DETAIL,
  APPEND_ITEMS,
  UPDATE_PAGE,
  SET_TITLE,
  SET_TEMPIMG,
  SET_TEMPTXT,
  HANDLE_TOGGLE,
} from './actionTypes';

// Helper impo

// eslint-disable-next-line import/prefer-default-export
export const SetDetail = item => ({
  type: SET_DETAIL,
  item,
});

export const SetTitle = item => ({
  type: SET_TITLE,
  item,
});

export const AppendItems = item => ({
  type: APPEND_ITEMS,
  item,
});

export const UpdatePage = () => ({
  type: UPDATE_PAGE,
});

export const SetImgData = () => ({
  type: SET_TEMPIMG,
});

export const SetTxtData = () => ({
  type: SET_TEMPTXT,
});

export const Togle = item => ({
  type: HANDLE_TOGGLE,
  item,
});

export function fetchProducts() {
  return (dispatch, getState) => {
    const page = getState().page + 1;

    const onSuccess = (data) => {
      const datas = data.data;
      dispatch(AppendItems(imgDataConverter(datas)));
      dispatch(UpdatePage());
    };

    const onError = (data) => {
      console.log(data);
    };
    return service(`https://picsum.photos/v2/list?page=${page}&limit=5`, onSuccess, onError);
  };
}

export function fetchDetail(id) {
  return dispatch => dispatch(SetDetail(imgOnly(`https://picsum.photos/id/${id}/500/500`, id)));
}
