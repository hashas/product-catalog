const SET_DETAIL = 'SET_DETAIL';
const SET_TITLE = 'SET_TITLE';
const APPEND_ITEMS = 'APPEND_ITEMS';
const UPDATE_PAGE = 'UPDATE_PAGE';
const HANDLE_TOGGLE = 'HANDLE_TOGGLE';

//
const SET_TEMPIMG = 'SET_TEMPIMG';
const SET_TEMPTXT = 'SET_TEMPTXT';

export {
  SET_DETAIL,
  APPEND_ITEMS,
  UPDATE_PAGE,
  SET_TITLE,
  SET_TEMPIMG,
  SET_TEMPTXT,
  HANDLE_TOGGLE,
};
