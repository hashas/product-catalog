import axios from 'axios';

// eslint-disable-next-line func-names
const service = function (URL, onSuccess, onError) {
  axios
    .get(URL, {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'Postman-Token': '765f785d-93d9-440f-9b71-f86c2c282016',
    })
    .then((response) => {
      // handle success
      console.log(response);
      onSuccess(response);
    })
    .catch((error) => {
      // handle error
      console.log(error);
      onError(error);
    })
    .finally(() => {
      // always executed
      console.log('...fetched');
    });
};

export default service;
