/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React from 'react';
import { Link } from 'react-router-dom';

// Import Global Styling //
import './cards.scss';

const Cards = (props) => {
  const query = `id=${props.id}`;
  return (
    <div className="cards-container">
      <Link
        to={{
          pathname: '/detail',
          search: query,
          state: { item: props.products },
        }}
      >
        <div className="img-wrapper">
          <div className="isContainer">
            {props.imgUrl}
            <img src={props.products.imgUrl} alt="img" />
          </div>
        </div>
      </Link>
      <div className="description-wrapper">
        <div className="left-container">
          <Link
            to={{
              pathname: '/detail',
              search: query,
              state: { item: props.products },
            }}
          >
            <p>{props.products.title}</p>
          </Link>
          <div className="size">{props.products.size}</div>
          <div className="price">{props.products.price}</div>
        </div>
        <div className="right-container">
          <button className="fav">
            <img
              src="https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-heart-grey-0a895ac5bdf1f98aa48d5f85efc7679d.png"
              alt="img"
            />
          </button>
          <button
            className="action"
            onClick={() =>
              props.handleToggle({
                urls: props.products.imgUrl,
                text: props.products.title,
                price: props.products.price,
              })
            }
          >
            <span>BELI</span>
          </button>
        </div>
      </div>
    </div>
  );
};

export default Cards;
