/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import
import { connect } from 'react-redux';
import React, { Component } from 'react';
import { Togle } from '../../store/actions/index';

// Import Global Styling //
import './confirmation.scss';

// Get data from store //
const mapStateToProps = state => ({
  productPreview: state.productPreview,
  isOpen: state.isOpen,
});

class Confirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleTogle() {
    this.props.dispatch(Togle());
  }

  render() {
    const classes = this.props.isOpen ? 'confirmation-container isOpen' : 'confirmation-container';

    const URL = this.props.productPreview ? this.props.productPreview.urls : '';
    const text = this.props.productPreview ? this.props.productPreview.text : '';
    const Price = this.props.productPreview ? this.props.productPreview.price : '';

    return (
      <div className={classes}>
        <div className="black-drop" onClick={() => this.handleTogle()} />
        <div className="contain">
          <div className="content-container">
            <div className="heading">
              <div className="img">
                <img src={URL} alt="images" />
              </div>
              <div className="text-wrap">
                <div className="stat">
                  <span>Coba Dulu</span>
                </div>
                <div className="text">{text}</div>
                <div className="texts">{Price}</div>
              </div>
            </div>
            <div className="body" />
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps)(Confirmation);
