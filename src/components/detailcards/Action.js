/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React from 'react';
// import { Link } from 'react-router-dom';

const Action = props => (
  <div className="container-list">
    <p className="small">3,133 Kali Disimpan</p>
    <div className="action-button">
      <button className="light" onClick={() => props.handleToggle()}>
        <img
          src="https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-heart-greyDark-74fa2bfb60cf3c250b268fc886ef3144.png"
          alt="logo"
        />
        <span>SIMPAN</span>
      </button>
      <button className="color" onClick={() => props.handleToggle(props.title)}>
        <span>BELI SEKRANG</span>
      </button>
    </div>
  </div>
);

export default Action;
