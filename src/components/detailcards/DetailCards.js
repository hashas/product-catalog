/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React, { Fragment } from 'react';
// import { Link } from 'react-router-dom';
import Carousel, { Modal, ModalGateway } from 'react-images';

// Import Components //
import Preview from './Preview';
import Material from './Material';
import Size from './Size';
import Action from './Action';
import Detail from './Detail';
import Strap from './Strap';

// Import Global Styling //
import './detailCards.scss';

const DetailCards = (props) => {
  const theurl = props.itemDetail ? props.itemDetail.imgUrl : '';
  const thetitle = props.itemDetail ? props.itemDetail.title : '';
  const theprice = props.itemDetail ? props.itemDetail.price : '';
  const thesize = props.itemDetail ? props.itemDetail.size : '';

  return (
    <Fragment>
      <div className="cards-container">
        <Strap />
        <a>
          <div className="img-wrapper" onClick={() => props.handleViewDetail()}>
            <div className="isContainer">
              <img src={theurl} alt="img" />
            </div>
          </div>
        </a>
        <Preview title={thetitle} price={theprice} handleToggle={() => props.handleToggle()} />
        <Material material="Katun Combad Grade 1" />
        <Size color="White" size={thesize} />
        <Action handleToggle={() => props.handleToggle()} />
        <Detail detail="Bahan : Katun stretch Detail Pants : Kancing depan, Resleting depan, dan Saku depan" />
      </div>

      <ModalGateway>
        {props.modalIsOpen ? (
          <Modal onClose={() => props.handleViewDetail()} closeOnBackdropClick>
            <Carousel views={[{ src: props.itemDetail.imgUrl }]} />
          </Modal>
        ) : null}
      </ModalGateway>
    </Fragment>
  );
};

export default DetailCards;
