/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React from 'react';
// import { Link } from 'react-router-dom';

const Strap = () => (
  <div className="outer-container">
    <div className="inner-container">
      <button className="button">
        <span className="qa_common_text" type="span">
          COBA DULU
        </span>
        <span className="qa_common_text" type="span">
          BARU BAYAR
        </span>
      </button>
    </div>
    <div className="inner-container sec">
      <div className="line" />
      <button>
        <span className="qa_common_text" type="span">
          COD
        </span>
        <span className="qa_common_text" type="span">
          SE-INDONESIA
        </span>
      </button>
      <div className="line" />
    </div>
    <div className="inner-container">
      <button>
        <span className="qa_common_text" type="span">
          GARANSI
        </span>
        <span className="qa_common_text" type="span">
          30 HARI
        </span>
      </button>
    </div>
  </div>
);

export default Strap;
