/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React from 'react';
// import { Link } from 'react-router-dom';

const Preview = props => (
  <div className="description-wrapper is-detail">
    <div className="left-container">
      <p>{props.title}</p>
      <div className="price">
        {props.price}
        <div className="testing" onClick={() => props.handleToggle()}>
          <img
            src="https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-cdbb-green-1bd5de1523af79f96b6da5f5339d22b8.png"
            alt="icon"
          />
          <p>Bisa Coba Dulu</p>
        </div>
      </div>
    </div>
  </div>
);

export default Preview;
