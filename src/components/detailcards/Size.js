/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React from 'react';
// import { Link } from 'react-router-dom';

const Size = props => (
  <div className="container-list">
    <p>{`${'Warna: '}${props.color}`}</p>
    <p>{props.size}</p>

    <button className="action-size">
      <img
        src="https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-ruler-greyDark-c49950d4e64e4814a4259115cc9a4603.png"
        alt="logo"
      />
      <p> Panduan Ukuran</p>
    </button>
  </div>
);

export default Size;
