/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React from 'react';
// import { Link } from 'react-router-dom';

const Material = props => (
  <div className="container-list">
    <p className="heading">BAHAN UTAMA</p>
    <p>{props.material}</p>
  </div>
);

export default Material;
