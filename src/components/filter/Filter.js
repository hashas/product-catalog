/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React, { Component } from 'react';

// Import Global Styling //
import './filter.scss';

class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="filter-container">
        <div className="select-wrapper">
          <select>
            <option value="">URUTKAN</option>
            <option value="personalized-desc">Rekomendasi</option>
            <option value="publish_start-desc">Terbaru</option>
            <option value="trending-desc">Trending</option>
            <option value="popular-desc">Terpopuler</option>
            <option value="topselling-desc">Terlaris</option>
            <option value="price-asc">Termurah</option>
            <option value="price-desc">Termahal</option>
          </select>
        </div>
        <button>FILTER</button>
      </div>
    );
  }
}

export default Filter;
