/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

const HeaderAction = props => (
  <Fragment>
    <div className="title-container">
      <div className="title">{props.titleCategory}</div>
    </div>
    <div className="action-header-container">
      <button>
        <Link to="/search">
          <img
            src="https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-search-greyDark-75485234b11747c9cab05016d0c0c4be.png"
            alt="img"
          />
        </Link>
      </button>
      <button>
        <img
          src="https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-heart-greyDark-74fa2bfb60cf3c250b268fc886ef3144.png"
          alt="img"
        />
      </button>
      <button>
        <img
          src="https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-cart-greyDark-8465cf6f65a64e2a8dbf6100d9f705d3.png"
          alt="img"
        />
      </button>
    </div>
  </Fragment>
);

export default HeaderAction;
