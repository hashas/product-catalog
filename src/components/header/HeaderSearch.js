/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React, { Fragment } from 'react';

const HeaderSearch = () => (
  <Fragment>
    <div className="action-header-container is-search">
      <div className="search">
        <div className="wrapper">
          <div className="img">
            <img
              src="https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-search-grey-d294714a85881c7bc240e883ce5b2135.png"
              alt="icon"
            />
          </div>
          <div className="input">
            <input type="text" placeholder="Cari apa Sis?" />
          </div>
        </div>
      </div>
      <button>
        <img
          src="https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-cart-greyDark-8465cf6f65a64e2a8dbf6100d9f705d3.png"
          alt="img"
        />
      </button>
    </div>
  </Fragment>
);

export default HeaderSearch;
