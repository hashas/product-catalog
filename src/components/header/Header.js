/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable react/prop-types */
// npm module import

import React, { Component } from 'react';

// Import Global Styling //
import './header.scss';

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <header className="srb-header">
        <div className="container-wrapper">
          <div className="back-button">
            <button onClick={() => window.history.back()}>
              <img
                src="https://salestock-public-prod.freetls.fastly.net/balok-assets/assets/img/icons/icon-arrow-left-greyDark-de6db57432bb3f61b5e30f988b59eef8.png"
                alt="img"
              />
            </button>
          </div>
          {this.props.children}
        </div>
      </header>
    );
  }
}

export default Header;
