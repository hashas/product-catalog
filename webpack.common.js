// -----------------------------------------------------------------------------
// This configuration file is a common config used in development and production
// You can make changes to this file to suit your development and production need
// Please refer to this webpack documentation for further information
// https://webpack.js.org/concepts/configuration/#simple-configuration
// -----------------------------------------------------------------------------

// --> Please make changes to these:
// Project and version dependent variables
const name = 'bundle';
const version = '1.1.0'; // <-- please refer to this link: https://semver.org/
// ---------------------------------------

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    filename: `js/${name}.${version}.js`,
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
  },
  module: {
    rules: [
      {
        test: /\.(gif|png|jpg|svg)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              useRelativePath: false,
              outputPath: 'img/',
            },
          },
          {
            loader: 'image-webpack-loader',
            options: {
              mozjpeg: {
                progressive: true,
                quality: 65,
              },
              optipng: {
                enabled: true,
              },
              gifsicle: {
                interlaced: false,
              },
            },
          },
        ],
      },
      {
        test: /\.(ico)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              context: path.resolve(__dirname, './src/img'),
              name: '[path][name].[ext]',
              useRelativePath: false,
              outputPath: 'img/',
            },
          },
        ],
      },
      {
        test: /\.scss$/,
        include: [path.resolve(__dirname, 'node_modules/font-awesome/scss/font-awesome.scss')],
        use: [
          {
            loader: 'style-loader', // creates style nodes from JS strings
          },
          {
            loader: 'css-loader', // translates CSS into CommonJS
          },
          {
            loader: 'sass-loader', // compiles Sass to CSS
          },
        ],
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015'],
          },
        },
      },
      {
        test: /\.woff2?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        // Limiting the size of the woff fonts breaks font-awesome ONLY for the extract text plugin
        // loader: "url?limit=10000"
        use: 'url-loader',
      },
      {
        test: /\.(ttf|eot|svg)(\?[\s\S]+)?$/,
        use: 'file-loader',
      },
    ],
  },
  resolve: {
    modules: ['node_modules', 'src'],
  },
  plugins: [
    new CleanWebpackPlugin(['dist']),
    new HtmlWebpackPlugin({ template: './src/index.tmpl.html' }),
  ],
};
