const autoprefixer = required('autoprefixer');

module.export = {
    plugins: [autoprefixer]
}