// -------------------------------------------------------------------
// This configuration file is for production use
// You can make changes to this file to suit your production need
// Please refer to this webpack documentation for further information
// https://webpack.js.org/guides/production/
// -------------------------------------------------------------------

// --> Please make changes to this constant:
// version dependent variable
const version = '1.0.0'; // <-- please refer to this link: https://semver.org/
// -----------------------------------------

const path = require('path');
const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const common = require('./webpack.common.js');
const autoprefixer = require('autoprefixer');

const extractCSS = new ExtractTextPlugin(`css/style.${version}.css`);

module.exports = merge(common, {
  module: {
    rules: [
      {
        test: /\.(scss)$/,
        exclude: [path.resolve(__dirname, 'node_modules/font-awesome/scss/font-awesome.scss')],
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader', // creates style nodes from JS strings
          use: [
            {
              loader: 'css-loader', // translates CSS into CommonJS
              options: {
                modules: true,
                localIdentName: '[name]__[local]___[hash:base64:5]',
              },
            },
            {
              loader: 'postcss-loader', // provide autoprefixer in compiled CSS
              options: {
                plugins: () => [autoprefixer],
              },
            },
            {
              loader: 'sass-loader', // compiles Sass to CSS
              options: {
                modules: true,
                localIdentName: '[name]__[local]___[hash:base64:5]',
              },
            },
          ],
        }),
      },
    ],
  },
  plugins: [extractCSS],
  mode: 'production',
  devtool: 'source-map',
});
